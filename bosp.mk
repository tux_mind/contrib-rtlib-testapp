
ifdef CONFIG_CONTRIB_TESTING_TESTAPP

# Targets provided by this project
.PHONY: rtlib_testapp clean_rtlib_testapp

# Add this to the "contrib_testing" target
testing: rtlib_testapp
clean_testing: clean_rtlib_testapp

MODULE_CONTRIB_TESTING_TESTAPP=contrib/testing/rtlib-testapp

rtlib_testapp: external
	@echo
	@echo "==== Building RTLib Test Application ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_CONTRIB_TESTING_TESTAPP)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_TESTING_TESTAPP)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_TESTING_TESTAPP)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_TESTING_TESTAPP)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_rtlib_testapp:
	@echo
	@echo "==== Clean-up RTLib Test Application ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/bbque-testapp ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/BbqRTLibTestApp*; \
		rm -f $(BUILD_DIR)/usr/bin/bbque-testapp*
	@rm -rf $(MODULE_CONTRIB_TESTING_TESTAPP)/build
	@echo

else # CONFIG_CONTRIB_TESTING_TESTAPP

rtlib_testapp:
	$(warning contib TestApp module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_TESTING_TESTAPP

