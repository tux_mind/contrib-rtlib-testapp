/**
 * Copyright (C) 2015  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RTLIB_TEST_EXC_H_
#define RTLIB_TEST_EXC_H_

#include <bbque/bbque_exc.h>

using bbque::rtlib::BbqueEXC;

class TestWorkload : public BbqueEXC {

public:

	TestWorkload(std::string const & name,
			std::string const & recipe,
			RTLIB_Services_t *rtlib,
			uint32_t cycle_ms,
			uint32_t cycles_total);

private:

	uint32_t cycle_ms;
	uint32_t cycle_length;
	uint32_t cycles_total;
	float n_core_max = 4;

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(int8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();

};

#endif // RTLIB_TEST_EXC_H_

